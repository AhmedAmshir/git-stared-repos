<?php

require_once __DIR__.'/vendor/autoload.php';

use App\Application;
use App\Client\GithubClient;
use App\Enums\GitHubEnum;

$app = new Application(new GithubClient(GitHubEnum::$github_search_url));
echo $app->get();