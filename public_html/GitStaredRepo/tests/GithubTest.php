<?php

namespace App\Test;

use Dotenv\Dotenv;
use GuzzleHttp\Client;
use App\Client\GithubClient;
use PHPUnit\Framework\TestCase;

class GithubTest extends TestCase
{
    private ?Client $http;

    public function setUp(): void
    {
        $dotenv = Dotenv::createImmutable(dirname(__DIR__, 1));
        $dotenv->load();

        $this->http = new Client([
            'base_uri' => $_ENV['BASE_URL']
        ]);
    }

    public function test_mock_get_git_popular_repos_successfully()
    {
        $param = 'q=created:>2021-01-10%20stars:>100%20language:php&sort=stars&order=desc';
        $data = json_decode(file_get_contents(__DIR__.'/result.json'), true);

        $github_mock = $this->createMock(GithubClient::class);

        $github_mock->method('getPopularRepos')
            ->with($param)
            ->willReturn($data);

        $this->assertEquals($data, $github_mock->getPopularRepos($param));
    }

    public function test_cli_to_git_return_proper_repos_without_any_query()
    {
        $response = shell_exec('composer get-stared-repos');
        $body = json_decode($response, true);
        $this->assertIsString('Validation Failed', $body['message']);
    }

    public function test_git_cli_return_proper_repos_without_q_query()
    {
        $response = shell_exec('composer get-stared-repos order=desc per_page=5');
        $body = json_decode($response, true);
        $this->assertIsString('Validation Failed', $body['message']);
    }

    public function test_cli_get_git_5_popular_repos_per_request()
    {
        $response = shell_exec('composer get-stared-repos created=gte:2020-01-10 per_page=5');
        $body = json_decode($response, true);
        $this->assertEquals(5, sizeof($body['data']));
    }

    public function test_cli_does_not_get_git_5_popular_repos_per_request()
    {
        $response = shell_exec('composer get-stared-repos created=gte:2020-01-10 per_page=6');
        $body = json_decode($response, true);
        $this->assertNotEquals(5, sizeof($body['data']));
    }

    public function test_cli_get_git_5_popular_repos_during_and_after_April_month()
    {
        $response = shell_exec('composer get-stared-repos created=gte:2021-04-01');
        $body = json_decode($response, true);

        $april_date = strtotime('2021-04-01 00:00:00');
        $expected_date = strtotime(date('Y-m-d H:i:s', strtotime($body['data'][2]['created_at'])));

        $this->assertGreaterThan($april_date, $expected_date);
    }

    public function test_cli_get_git_popular_repos_greater_than_500_stars()
    {
        $response = shell_exec('composer get-stared-repos stars=gt:500 created=gte:2020-01-10',);

        $body = json_decode($response, true);
        $this->assertGreaterThan(500, $body['data'][0]['stars']);
        $this->assertIsString('Get data successfully', $body['message']);
    }

    public function test_git_api_return_proper_repos_without_any_query()
    {
        $response = $this->http->request('GET', 'index.php', ['http_errors'=>false]);

        $this->assertEquals(422, $response->getStatusCode());

        $body = json_decode($response->getBody()->getContents(), true);
        $this->assertIsString('Validation Failed', $body['message']);
    }

    public function test_git_api_return_proper_repos_without_q_query()
    {
        $response = $this->http->request('GET', 'index.php?order=desc&per_page=5', ['http_errors'=>false]);

        $this->assertEquals(422, $response->getStatusCode());

        $body = json_decode($response->getBody()->getContents(), true);
        $this->assertIsString('Validation Failed', $body['message']);
    }

    public function test_get_git_popular_repos_greater_than_500_stars()
    {
        $response = $this->http->request('GET', 'index.php?stars=gt:500&created=gte:2020-01-10', ['http_errors'=>false]);

        $this->assertEquals(200, $response->getStatusCode());

        $body = json_decode($response->getBody()->getContents(), true);
        $this->assertGreaterThan(500, $body['data'][0]['stars']);
        $this->assertIsString('Get data successfully', $body['message']);
    }

    public function test_get_git_5_popular_repos_per_request()
    {
        $response = $this->http->request('GET', 'index.php?created=gte:2020-01-10&per_page=5', ['http_errors'=>false]);

        $this->assertEquals(200, $response->getStatusCode());
        $body = json_decode($response->getBody()->getContents(), true);
        $this->assertEquals(5, sizeof($body['data']));
    }

    public function tearDown(): void
    {
        $this->http = null;
    }
}