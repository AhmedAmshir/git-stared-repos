<?php

namespace App\Client;

interface IClient
{
    public function prepareUrlQuery(array $request);

    public function getPopularRepos(string $params);
}