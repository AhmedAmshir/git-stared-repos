<?php

namespace App\Handler;

use App\Enums\ResponseEnum;

trait ResponseHandler
{
    private array $data = [];

    public function addParameter(array $value): void
    {
        if (isset($value) && !empty($value)) {
            $this->data = $value;
        }
    }

    public function ok(string $message = ''): string
    {
        http_response_code(ResponseEnum::$statusCodes['OK']['id']);
        return $this->createResponse(ResponseEnum::$statusCodes['OK']['id'], $message);
    }

    public function badRequest(string $message = ''): string
    {
        http_response_code(ResponseEnum::$statusCodes['UnprocessableEntity']['id']);
        return $this->createResponse(ResponseEnum::$statusCodes['UnprocessableEntity']['id'], $message);
    }

    private function createResponse(int $status_code, string $message): string
    {

        $return['status_code'] = $status_code;

        $return['message'] = '';
        if (isset($message) && !empty($message)){
            $return['message'] = $message;
        }

        $return['data'] = $this->data;

        header("Content-Type: application/json");
        return json_encode($return, JSON_PRETTY_PRINT);
    }
}