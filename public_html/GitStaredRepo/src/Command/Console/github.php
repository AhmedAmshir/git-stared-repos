#!/usr/bin/php
<?php

require_once dirname(__DIR__, 3).'/vendor/autoload.php';

use App\Application;
use App\Enums\GitHubEnum;
use App\Client\GithubClient;

$params = [];
unset($argv[0]);
foreach($argv as $arg)
{
    $param = explode('=', $arg);
    $params[$param[0]] = $param[1];
}

$app = new Application(new GithubClient(GitHubEnum::$github_search_url));
echo $app->get($params).PHP_EOL;
