<?php

namespace App\Enums;

class GitHubEnum
{
    public static string $default_user_agent = "Mozilla/5.0";

    public static string $github_search_url = "https://api.github.com/search/repositories?";

    public static array $filters = ['sort', 'order', 'per_page', 'page'];

    public static array $orders = [
        'e' => ':',
        'gt' => ':>',
        'gte' => ':>=',
        'lte' => ':<=',
        'lt' => ':<',
    ];
}