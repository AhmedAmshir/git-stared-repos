<?php

namespace App;

use App\Client\IClient;
use App\Handler\ResponseHandler;

class Application
{
    use ResponseHandler;

    private IClient $client;

    /**
     * Application constructor.
     * @param IClient $client
     */
    public function __construct(IClient $client)
    {
        $this->client = $client;
    }

    public function get(array $query = []): string
    {
        $params = $query ? $query : $_GET;
        $query = $this->client->prepareUrlQuery($params);

        if(!$query)
            return $this->badRequest('Validation Failed');

        $data = $this->client->getPopularRepos($query);

        $this->addParameter($data);
        return $this->ok('Get data successfully');
    }
}