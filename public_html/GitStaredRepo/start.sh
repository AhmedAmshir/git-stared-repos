docker-compose up -d --build
docker-compose exec php-fpm composer install
docker-compose exec php-fpm cp .env.example .env
docker-compose exec php-fpm chmod -R 777 .env

echo "Project installed successfully";